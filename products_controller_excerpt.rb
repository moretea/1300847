@products = Product.search(params[:q], :page => paginate_page, :per_page => paginate_per_page).
            without_hidden_members.
            using_sort_mode(params[:sort]).
            with_sphinx_state(:published)